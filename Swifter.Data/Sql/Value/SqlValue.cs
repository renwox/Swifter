﻿namespace Swifter.Data.Sql
{
    /// <summary>
    /// 表示一个 SQL 值
    /// </summary>
    /// <typeparam name="T">值类型</typeparam>
    public abstract class SqlValue<T> : IValue
    {
        /// <summary>
        /// 值
        /// </summary>
        public abstract T Value { get; }
    }
}