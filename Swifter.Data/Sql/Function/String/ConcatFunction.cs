﻿namespace Swifter.Data.Sql
{
    /// <summary>
    /// 拼接字符串函数。
    /// </summary>
    public sealed class ConcatFunction : UnaryFunction
    {
        private static readonly SqlValue<IValue[]> Empty = new ConstantValue<IValue[]>(new IValue[0]);

        /// <summary>
        /// 构建拼接字符串函数。
        /// </summary>
        /// <param name="value">值数组</param>
        public ConcatFunction(SqlValue<IValue[]> value) : base(value)
        {
        }

        /// <summary>
        /// 值。
        /// </summary>
        new public SqlValue<IValue[]> Value => (base.Value as SqlValue<IValue[]>) ?? Empty;
    }
}