﻿namespace Swifter.Api
{
    /// <summary>
    /// 要求客户端刷新 Token。
    /// </summary>
    public sealed class ReTokenException : BaseException
    {
        public ReTokenException(string message) : base(ReturnCodes.ReToken, message)
        {
        }
    }
}
