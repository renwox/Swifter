﻿namespace Swifter.Api
{
    /// <summary>
    /// 参数异常。
    /// </summary>
    public sealed class ParameterException : BaseException
    {
        public ParameterException(string parameterName, ParameterExceptionCode exceptionCode) : base(ReturnCodes.Error, "Parameter exception")
        {
            ReturnData = new { ParameterName = parameterName, ExceptionCode = exceptionCode };
        }
    }
}
