﻿namespace Swifter.Api
{
    /// <summary>
    /// 参数异常类型。
    /// </summary>
    public enum ParameterExceptionCode
    {
        CannotBeNull = 1,
        Incorrect = 2
    }
}
