﻿using Swifter.RW;

namespace Swifter.Api.Types
{
    sealed class GlobalParameter : ITypeInfo, IValidator, IValueInterface<GlobalParameter>
    {
        public object value;

        public GlobalParameter ReadValue(IValueReader valueReader)
        {
            return new GlobalParameter { value = valueReader.DirectRead() };
        }

        public void Verify(ContextInfo contextInfo, ContextParameters parameters, string name)
        {
            value = contextInfo.Application.GetGlobalParameter(name).Value;
        }

        public void WriteValue(IValueWriter valueWriter, GlobalParameter value)
        {
            ValueInterface.WriteValue(valueWriter, value.value);
        }
    }
}