﻿namespace Swifter.Api
{
    /// <summary>
    /// 要求客户端更换服务器。
    /// </summary>
    public sealed class ReApplicationException: BaseException
    {
        public ReApplicationException(string message) : base(ReturnCodes.ReApplication, message)
        {
        }
    }
}
