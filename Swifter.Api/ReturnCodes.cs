﻿namespace Swifter.Api
{
    /// <summary>
    /// 返回值代码。
    /// </summary>
    public enum ReturnCodes : int
    {
        Error = -1,
        Ok = 0,
        Unauthorized = 1,
        ReToken = 2,
        ReApplication = 3,
        ReTime = 4,
        VerifyClient = 5
    }
}
