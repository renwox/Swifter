﻿using System;

namespace Swifter.Api
{
    /// <summary>
    /// 开发者异常。
    /// </summary>
    public sealed class DeveloperException : Exception
    {
        public DeveloperException(string message) : base(message)
        {
        }
    }
}
