﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Diagnostics;

namespace Swifter.Api
{
    public static class Helper
    {
        public static bool GetDickInfo(DirectoryInfo directory, out long totalSize, out long usedSize)
        {
            var root = directory.Root;

            foreach (var drive in DriveInfo.GetDrives())
            {
                if (root.FullName == drive.RootDirectory.FullName)
                {
                    totalSize = drive.TotalSize;
                    usedSize = drive.TotalSize - drive.TotalFreeSpace;

                    return true;
                }
            }

            totalSize = 0;
            usedSize = 0;

            return false;
        }

        public static bool GetMemoryInfo(out long totalSize, out long usedSize)
        {
            throw new NotFiniteNumberException();
        }
    }
}
