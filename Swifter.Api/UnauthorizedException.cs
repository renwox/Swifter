﻿namespace Swifter.Api
{
    /// <summary>
    /// 未授权异常。
    /// </summary>
    public sealed class UnauthorizedException : BaseException
    {
        public UnauthorizedException(string message) : base(ReturnCodes.Unauthorized, message)
        {

        }
    }
}
