﻿using System;

namespace Swifter.Api
{
    public interface ICommandProcessor
    {
        RuntimeCommandInfo Build(BaseApplication application, CommandInfo commandInfo);

        CommandExtendedInfo[] ExtendedInfos { get; }
    }

    public sealed class CommandExtendedInfo
    {
        public string Name { get; set; }

        public Type Type { get; set; }
    }
}
