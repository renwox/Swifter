﻿// document.domain = "localhost";

app.version = 152;
app.url = getUrl();
app.client_id = "DA83D82A883E4324968B6D304CD4C415";
app.device_id = app.getStorage(Consts.deviceId);
app.token = app.getStorage(Consts.token);

if (!app.device_id) {
    app.makeDeviceId();
}

if (!app.token) {
    app.makeToken();
}

function getUrl() {

    var ports = { "http:": 8888, "https:": 88889 };

    var protocol = window.location.protocol;
    var hostname = window.location.hostname;
    var port = ports[protocol];
    var pathname = "/api/";

    // TODO: 47.106.146.13

    hostname = "localhost";

    return protocol + "//" + hostname + ":" + port + pathname;
}